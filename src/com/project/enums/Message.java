package com.project.enums;

public enum Message {
    HELLO("Добрый день!\nНеобходимо выбрать словарь для дальнейшей работы.\n1 - английский\n2 - цифры\n"),
    ACTIONS("Выберете действие:\n\n1 - просмотр словаря\n2 - удаление записи\n3 - поиск записи\n4 - добавление записи\n0 - выход\n"),
    ERROR("Введено некорректное значение\n"),
    ENTER_KEY("Введите ключ\n"),
    ENTER_VALUE("Введите значение\n"),
    SUCCESS("Успешно\n"),
    ADD_FAILED("Запись не была добавлена, т.к. ранее запись уже была добавлена, либо указан некорректный ключ\n"),
    NO_SUCH_ELEMENT("Элемента не обнаружено\n"),
    EMPTY_DICTIONARY("Файл не обнаружен, словарь пустой\n");

    private final String textMessage;

    Message(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getTextMessage() {
        return textMessage;
    }
}
