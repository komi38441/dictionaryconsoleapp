package com.project.enums;

import java.util.Arrays;
import java.util.Objects;

public enum Action {
    EXIT("0"),
    VIEW("1"),
    DELETE("2"),
    SEARCH("3"),
    ADD("4");

    private final String numberAction;

    public static Action getActionByNumberAction(String numberAction) {
        return Arrays.stream(Action.values())
                .filter(action -> Objects.equals(action.numberAction, numberAction))
                .findFirst().orElse(null);
    }

    Action(String numberAction) {
        this.numberAction = numberAction;
    }
}
