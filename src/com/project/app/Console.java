package com.project.app;

import com.project.enums.Action;
import com.project.model.dictionary.DigitDictionaryMap;
import com.project.model.dictionary.WordDictionaryMap;
import com.project.service.DictionaryMap;

import java.util.Scanner;

import static com.project.enums.Message.*;

public class Console {
    public static void main(String[] args) {
        Console console = new Console();
        console.run();
    }
    private void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            println(HELLO.getTextMessage());

            //первый этап - выбор словаря
            DictionaryMap<String, String> dictionaryMap = getDictionaryService(scanner);

            //выбор дальнейших действий (пока не будет введен 0 для выхода)
            mainMenu(scanner, dictionaryMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void mainMenu(Scanner scanner, DictionaryMap<String, String> dictionaryMap) {
        boolean exit = false;
        while (!exit) {
            println(ACTIONS.getTextMessage());
            try {
                Action action = Action.getActionByNumberAction(scanner.next());
                if(action != null) {
                    switch (action) {
                        case EXIT:
                            exit = true;
                            break;
                        case VIEW:
                            var dictionary = dictionaryMap.readFile();
                            println(dictionary.isEmpty() ? EMPTY_DICTIONARY.getTextMessage() : dictionary);
                            break;
                        case DELETE:
                            println(ENTER_KEY.getTextMessage());
                            println(dictionaryMap.remove(scanner.next()) != null ?
                                    SUCCESS.getTextMessage() :
                                    NO_SUCH_ELEMENT.getTextMessage());
                            break;
                        case SEARCH:
                            println(ENTER_KEY.getTextMessage());
                            String searchResult = dictionaryMap.get(scanner.next());
                            println(searchResult == null ? NO_SUCH_ELEMENT.getTextMessage() : searchResult);
                            break;
                        case ADD:
                            println(ENTER_KEY.getTextMessage());
                            String key = scanner.next();
                            println(ENTER_VALUE.getTextMessage());
                            String value = scanner.next();

                            println(dictionaryMap.put(key, value) != null ?
                                    SUCCESS.getTextMessage() :
                                    ADD_FAILED.getTextMessage());
                            break;
                        default:
                            break;
                    }
                }
                else {
                    println(ERROR.getTextMessage());
                }
            } catch (NumberFormatException e) {
                println(ERROR.getTextMessage());
            }
        }
    }
    private DictionaryMap<String, String> getDictionaryService(Scanner scanner) {
        DictionaryMap<String, String> dictionaryMap = null;

        while (dictionaryMap == null) {
            try {
                int numberAction = Integer.parseInt(scanner.next());
                switch (numberAction) {
                    case 1:
                        dictionaryMap = new WordDictionaryMap();
                        break;
                    case 2:
                        dictionaryMap = new DigitDictionaryMap();
                        break;
                    default:
                        println(ERROR.getTextMessage());
                        break;
                }
            } catch (NumberFormatException e) {
                println(ERROR.getTextMessage());
            }
        }
        return dictionaryMap;
    }
    public static void println(Object format, Object... args) {
        System.out.printf(format.toString(), args);
        System.out.println();
    }
}
