package com.project.service;

import java.util.Map;

public interface DictionaryMap<K, V> extends Map<K, V> {

    Map<String, String> readFile();
}