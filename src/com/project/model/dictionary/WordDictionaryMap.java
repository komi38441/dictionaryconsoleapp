package com.project.model.dictionary;

import com.project.model.AbstractDictionaryMap;

public class WordDictionaryMap extends AbstractDictionaryMap {

    private static final String FILE_NAME = "1.txt";

    public WordDictionaryMap() {
        super(FILE_NAME);
    }

    @Override
    protected boolean keyIsCorrect(String key) {
        return key.matches("[a-zA-Z]{4}");
    }
}
