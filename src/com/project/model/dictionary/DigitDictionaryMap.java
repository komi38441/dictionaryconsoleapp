package com.project.model.dictionary;

import com.project.model.AbstractDictionaryMap;

public class DigitDictionaryMap extends AbstractDictionaryMap {

    private static final String FILE_NAME = "2.txt";

    public DigitDictionaryMap() {
        super(FILE_NAME);
    }

    @Override
    protected boolean keyIsCorrect(String key) {
        return key.matches("[0-9]{5}");
    }
}
