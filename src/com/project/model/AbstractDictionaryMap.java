package com.project.model;

import com.project.service.DictionaryMap;

import java.io.*;
import java.util.HashMap;

import static com.project.app.Console.println;
import static com.project.enums.Message.EMPTY_DICTIONARY;

public abstract class AbstractDictionaryMap extends HashMap<String, String> implements DictionaryMap<String, String> {

    private final String fileName;
    private static final String SEPARATOR = ":";

    public AbstractDictionaryMap(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public HashMap<String, String> readFile() {
        HashMap<String, String> dictionary = new HashMap<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] lineElements = line.split(SEPARATOR);
                if (lineElements.length == 2 && keyIsCorrect(lineElements[0])) {
                    dictionary.put(lineElements[0], lineElements[1]);
                }
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            println(EMPTY_DICTIONARY.getTextMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dictionary;
    }

    @Override
    public String remove(Object key) {
        HashMap<String, String> oldDictionary = readFile();
        // если элемент по ключу был обнаружен - пересоздание файла с новым словарем
        String oldKey = oldDictionary.remove(key);
        if (oldKey != null) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                for (var entry : oldDictionary.entrySet()) {
                    writer.newLine();
                    writer.write(getDictionaryLine(entry.getKey(), entry.getValue()));
                }
                return oldKey;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return oldKey;
    }

    @Override
    public String get(Object key) {
        HashMap<String, String> dict = readFile();
        return dict.get(key);
    }

    @Override
    public String put(String key, String value) {
        HashMap<String, String> dictionary = readFile();
        if (keyIsCorrect(key)) {
            if (!dictionary.containsKey(key)) {
                String line = getDictionaryLine(key, value);
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
                    writer.newLine();
                    writer.write(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return dictionary.put(key, value);
    }

    /**
     * Метод, отвественный за соблюдение условий добавления записей
     *
     * @param key ключ
     * @return true, если условия соблюдены, false - не соблюдены
     */
    protected abstract boolean keyIsCorrect(String key);

    private static String getDictionaryLine(String key, String value) {
        return key + SEPARATOR + value;
    }
}